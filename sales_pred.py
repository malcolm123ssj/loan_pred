import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn.ensemble import AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor
import seaborn as sns

dataset = pd.read_csv('sales_pred.txt')
dataset.drop(['Item_Identifier','Outlet_Identifier'], axis = 1, inplace = True)

dataset.isnull().sum()

# =============================================================================
# pp = sns.pairplot(dataset, hue = 'Item_Fat_Content')
# dataset.boxplot('Item_Outlet_Sales','Outlet_Size',rot = 30,figsize = (10,10))
# dataset.boxplot('Item_Outlet_Sales','Outlet_Type',rot = 30,figsize = (10,10))
# =============================================================================

labelencoder_X = LabelEncoder()
dataset['Item_Fat_Content'] = labelencoder_X.fit_transform(dataset['Item_Fat_Content'].astype(str))
dataset['Outlet_Size'] = labelencoder_X.fit_transform(dataset['Outlet_Size'].astype(str))
dataset['Item_Type'] = labelencoder_X.fit_transform(dataset['Item_Type'].astype(str))
dataset['Outlet_Location_Type'] = labelencoder_X.fit_transform(dataset['Outlet_Location_Type'].astype(str))
dataset['Outlet_Type'] = labelencoder_X.fit_transform(dataset['Outlet_Type'].astype(str))

X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,[9]].values

imputer = Imputer(missing_values = 'NaN', strategy = 'median', axis = 0)
X[:,[0]] = imputer.fit_transform(X[:,[0]])
imputer = Imputer(missing_values = 3, strategy = 'most_frequent', axis = 0)
X[:, [6]] = imputer.fit_transform(X[:, [6]])

df_X = pd.DataFrame(X, columns = ['Item_Weight','Item_Fat_Content','Item_Visibility','Item_Type','Item_MRP','Outlet_Establishment_Year','Outlet_Size','Outlet_Location_Type','Outlet_Type'])
df_X = pd.get_dummies(df_X, columns = ['Item_Fat_Content','Item_Type','Outlet_Size','Outlet_Location_Type','Outlet_Type'], drop_first = True)

X1 = df_X.iloc[:,:]

scaler = MinMaxScaler()
X1 = scaler.fit_transform(X1)

X_train, X_test, y_train, y_test = train_test_split(X1, y, test_size = 0.3, random_state = 42)

dec_tree = DecisionTreeRegressor(max_features = 'sqrt', presort = True, random_state = 42, max_depth = 8)
dec_tree.fit(X_train, y_train)
y_pred = dec_tree.predict(X_test)

dec_tree.score(X_train,y_train) 

print("Mean squared error: " + str(np.mean(y_pred - y_test) ** 2))
print('Variance score: ' + str(lin_reg.score(X_test, y_test)))

regr = AdaBoostRegressor(DecisionTreeRegressor(max_depth=10), random_state=42, n_estimators=100, learning_rate = 0.55, loss = 'square')
regr.fit(X_train, y_train)  

regr.score(X_train, y_train)  